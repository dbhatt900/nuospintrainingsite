angular.module( 'constants', [] ).constant( 'Constants', {
  APP_URL: 'http://localhost:9090/api',
  AUTH_TOKEN_KEY: 'X-AUTH-TOKEN',
  CHANNEL: {
    USER_LOGIN: 'user'
  }
} );
