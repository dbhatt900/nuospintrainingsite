angular.module( 'constants', [] ).constant( 'Constants', {
  APP_URL: 'production server ip',
  AUTH_TOKEN_KEY: 'X-AUTH-TOKEN',
  CHANNEL: {
    USER_LOGIN: 'user'
  }
} );
