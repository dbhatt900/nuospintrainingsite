angular.module( 'app', [ 'ui.router', 'constants', 'ngMessages', 'ngMaterial',
  'LocalStorageModule', 'ladda',
] ).config( function( $locationProvider, $urlRouterProvider, $httpProvider,Constants ) {
  $locationProvider.html5Mode( true );
} ).run( function() {} );