

'use strict';

/********************************
Dependencies
********************************/
var express = require('express');// server middleware
var mongoose = require('mongoose');// MongoDB connection library
var bodyParser = require('body-parser');// parse HTTP requests



/********************************
Express Settings
********************************/
var app = express();
var dir = __dirname;
dir  = dir.replace("/api","");
dir = dir+ "/frontend/app";
app.use(express.static(dir));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));



/********************************
 Routing
 ********************************/

// Home
app.get('/', function (req, res){
    res.sendFile(dir +'/js/index.html');
});

/********************************
Ports
********************************/
app.listen(9090, function() {
  console.log("Node server running on " + 9090);
});
